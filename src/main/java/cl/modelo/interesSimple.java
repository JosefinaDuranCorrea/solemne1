/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

public class interesSimple {

    private int capital;
    private double tasa;
    private int anos;
    private double intereSimple;

    public int getCapital() {
        return capital;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public int getAnos() {
        return anos;
    }

    public void setAnos(int anos) {
        this.anos = anos;
    }

    public float getIntereSimple() {
        float Simple = (float) ((this.capital * this.tasa) * this.anos);
        return Simple;
    }

    public void setIntereSimple(double intereSimple) {
        this.intereSimple = intereSimple;
    }

}
