package cl.controller;

import cl.modelo.interesSimple;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Josefina
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class controller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        double capital = Integer.parseInt(request.getParameter("capital"));
        double tasa = Double.parseDouble(request.getParameter("tasainteres"));
        int anos = Integer.parseInt(request.getParameter("anos"));

        interesSimple interesSimple = new interesSimple();
        interesSimple.setCapital((int) capital);
        interesSimple.setTasa(tasa);
        interesSimple.setAnos(anos);

        System.out.println("Capital:" + capital);
        System.out.println("tasa de interes:" + tasa);
        System.out.println("Años:" + anos);
        System.out.println("Interes simple:" + interesSimple.getIntereSimple());

        request.setAttribute("interesSimple", interesSimple);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
