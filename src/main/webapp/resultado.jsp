

<%@page import="java.text.DecimalFormat"%>
<%@page import="cl.modelo.interesSimple"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    interesSimple interesSimple = (interesSimple) request.getAttribute("interesSimple");
    DecimalFormat formato = new DecimalFormat("#,###"); //Formato para números
    int valorCapital = interesSimple.getCapital();
    int valorInteres = (int) interesSimple.getIntereSimple();
%> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <style type="text/css">

            body {
                background-image: url("https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-full-white-technology-blue-square-background-technologythree-dimensional-squareelectronic-circuit-image_77487.jpg");
            }      


        </style>
        <title>Datos</title>

    </head>

    <body>

        <h2 class="textos">Capital: $ <%= formato.format(valorCapital)%>
            <br>
            <br>
            Tasa de interes: <%= interesSimple.getTasa()%>
            <br>
            <br>
            Años:<%= interesSimple.getAnos()%>
            <br>
            <br>
            Interes simple: $ <%= formato.format(valorInteres)%>
            <br>

            <a href="https://slemne01.herokuapp.com/" class="btn btn-success">Volver</a>
            .

    </body>
</html>
