<%-- 
    Document   : index
    Created on : 24/04/2021, 01:37:49 PM
    Author     : Josefina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <style type="text/css">
            *{box-sizing:border-box;}

            body {
                background-image: url("https://png.pngtree.com/thumb_back/fh260/background/20200604/pngtree-cute-hand-drawn-style-math-education-blue-plaid-background-image_337576.jpg");
            }      

            *{box-sizing:border-box;}

            form{
                width:300px;
                padding:16px;
                border-radius:10px;
                margin:auto;
                background-color:#ccc;
            }

            form label{
                width:72px;
                font-weight:bold;
                display:inline-block;
            }

            form input[type="text"],
            form input[type="email"]{
                width:180px;
                padding:3px 10px;
                border:1px solid #f6f6f6;
                border-radius:3px;
                background-color:#f6f6f6;
                margin:8px 0;
                display:inline-block;
            }

            form input[type="submit"]{
                width:100%;
                padding:8px 16px;
                margin-top:32px;
                border:1px solid #000;
                border-radius:5px;
                display:block;
                color:#fff;
                background-color:#000;
            } 

            form input[type="submit"]:hover{
                cursor:pointer;
            }

            textarea{
                width:100%;
                height:100px;
                border:1px solid #f6f6f6;
                border-radius:3px;
                background-color:#f6f6f6;			
                margin:8px 0;
                /*resize: vertical | horizontal | none | both*/
                resize:none;
                display:block;
            }
        </style>
    </head>

    <body>

    </head>
<body>

    <form  name="form" action="controller" method="GET">
        <h1>Ingreso de Datos</h1>
        
        <label for="capital">Capital: </label>

        <input type="text" name="capital" id="capital">

        <br/> <br/>


        <label for="tasainteres">Tasa de Interes: </label>

        <input type="text" name="tasainteres" id="tasainteres">

        <br/> <br/>

        <label for="años">N° de Años: </label>

        <input type="text" name="anos" id="anos">

        <br/> <br/>


        <button type="submit" class="btn btn-success">Enviar</button>

</form>

</body>
</html>
